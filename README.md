using pipenv for this. To install it please use:  
`pip install pipenv`

after change to the project dir, then:  
`pipenv install --three`

on Ubuntu I had to install tesseract with:  
`sudo apt-get install tesseract-ocr`

once done, you start the pipenv shell and install pytesseract and Pillow:  
`pipenv shell`  
`pipenv install pytesseract Pillow`  

to run the project just type:  
`python3 test.py`