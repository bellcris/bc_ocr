# install:
# sudo apt-get install tesseract-ocr

try:
    # from PIL import Image
    import PIL
except ImportError:
    import Image
import pytesseract
import re, os


def ocr_core(fname):
    text = pytesseract.image_to_string(PIL.Image.open(fname))
    return text

cut = False

if cut:
    for x in os.listdir('.'):
        if re.match(r'.*\.png|.*\.jpeg$', x):
            print('FOR img {} \n'.format(x))
            new_h = int(image.height * .2)
            start_h = image.height - new_h
            image_cut = image.crop((0, start_h, image.width, image.height))
            image_cut.save('c_{}'.format(x))
            image = PIL.Image.open(x)

for x in os.listdir('.'):
    if re.match(r'B.*\.png|B.*\.jpeg$', x):
        print('>>>>>>>>> FOR img {} <<<<<<<<<\n'.format(x))
        print(ocr_core(x) + '\n')
